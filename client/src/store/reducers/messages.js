import {GET_MESSAGES_REQUEST, GET_MESSAGES_SUCCESS} from "../actions/actionTypes";

const initialState = {
    messages: [],
    loading: true
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MESSAGES_REQUEST:
            return {...state, loading: true};
        case GET_MESSAGES_SUCCESS:
            return {...state,  loading: false, messages: action.messages};
        default:
            return state;
    }
};

export default reducer;