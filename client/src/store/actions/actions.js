import {GET_MESSAGES_REQUEST, GET_MESSAGES_SUCCESS} from "./actionTypes";
import axios from '../../axios-api';

export const getMessagesRequest = (data) => {
    return {type: GET_MESSAGES_REQUEST, messages: data}
};
export const getMessagesSuccess = (data) => {
    return {type: GET_MESSAGES_SUCCESS, messages: data}
};

export const getMessages = () => {
    return dispatch => {
        dispatch(getMessagesRequest());
        axios.get('/messages').then(res => {
            dispatch(getMessagesSuccess(res.data));
        });
    }
};