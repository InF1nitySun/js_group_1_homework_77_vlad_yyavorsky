import React, {Component} from 'react';
import './App.css';
import SendMessages from "./containers/SendMessages/SendMessages";
import ListOfMessages from "./containers/ListOfMessages/ListOfMessages";
import {connect} from 'react-redux';
import {getMessages} from "./store/actions/actions";


class App extends Component {
    componentDidMount() {
        setInterval(() => {
            this.props.getMessages();
        }, 2000);
    }

    render() {
        return (
            <div className="container" id="App">
                <h4>HomeWork_77</h4>
                <ListOfMessages messages={this.props.messages} loading={this.props.loading}/>
                <SendMessages/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {messages: state.messages, loading: state.loading};
};

const mapDispatchToProps = dispatch => {
    return {getMessages: () => dispatch(getMessages())};
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
