import React from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import './SendMessages.css';
import axios from '../../axios-api';

class SendMessages extends React.Component {
    state = {
        author: '',
        message: '',
        image: ''
    };

    changeDataHandler = event => {
        this.setState({[event.target.name]: event.target.value})
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    sendData = e => {
        e.preventDefault();
        if (this.state.message === '' || this.state.message === undefined) {
            console.log("Message must be present in the request");

            alert("Message must be present in the request");

        } else {

            const formData = new FormData();

            Object.keys(this.state).forEach(key => {
                formData.append(key, this.state[key]);
            });

            axios.post('/messages', formData).then(() => {
                this.setState({author: '', message: ''});
            }).catch(error => {
                console.log(error);
            })
        }
    };

    render() {
        return (
            <Form inline>
                <FormGroup controlId="formInlineName">
                    <ControlLabel className="author">Name</ControlLabel>
                    <FormControl className="authorInput" required type="text" placeholder="Author" name="author"
                                 onChange={this.changeDataHandler} value={this.state.author}/>
                </FormGroup>
                <FormGroup>
                    <ControlLabel className="message">Message</ControlLabel>
                    <FormControl className="messageInput" required type="text" placeholder="Enter your message"
                                 name="message" onChange={this.changeDataHandler} value={this.state.message}/>
                </FormGroup>
                <FormGroup controlId="productImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl placeholder="Add file" type="file" name="image"
                                     onChange={this.fileChangeHandler}/>
                    </Col>
                </FormGroup>
                <Button type="submit" onClick={this.sendData}>Send</Button>
            </Form>
        )
    }
}

export default SendMessages;