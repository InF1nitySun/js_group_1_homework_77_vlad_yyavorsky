import React from 'react';
import Moment from 'react-moment';
import {Col, Image, ListGroup, ListGroupItem} from "react-bootstrap";

import {apiURL} from '../../constants';

import './ListOfMessages.css';


const ListOfMessages = props => {

    //
    return (
        <ListGroup>
            {props.messages.map(message => (
                    <ListGroupItem key={message.id} style={{"minHeight":"125px"}}>
                        {message.image ? <Col md={2}>
                            <Image src={`${apiURL}/uploads/${message.image}`} thumbnail />
                        </Col> : null}
                        <Col>
                            <h5 className="name">{message.author}</h5>
                            <Moment className="date" format="YYYY/MM/DD HH:mm">{message.date}</Moment>
                            {message.message}
                        </Col>
                    </ListGroupItem>
                )
            ).reverse()}
        </ListGroup>
    )
};

export default ListOfMessages;