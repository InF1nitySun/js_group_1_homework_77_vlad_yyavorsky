const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {

    router.get('/', (req, res) => {
        if (req.query.date) {

            let date = req.query.date;
            res.send(db.getDataByDataTime(date));
        } else if (req.query.id) {

            let id = req.query.id;
            res.send(db.getDataById(id));
        } else {

            let result = db.getData().slice(-30);
            res.send(result);
        }

    });

    router.post('/', upload.single('image'),(req, res) => {
        const product = req.body;

        if (req.file) {
            product.image = req.file.filename;
        }else{
            product.image = null;
        }

        if (product.author === '' || product.author === undefined) {
            product.author = 'Anonymous';
        }

        if (product.message === '' || product.message === undefined) {

            console.log("Message must be present in the request");

            res.status(400).send({"error": "Message must be present in the request"});
        } else {
            db.addItem(product).then(result => {
                res.send(result);
            });
        }
    });

    return router;
};

module.exports = createRouter;