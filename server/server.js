const express = require('express');
const fileDb = require('./fileDb');
const products = require('./app/products');
const app = express();
const cors = require('cors');

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));



fileDb.init().then(() => {
    console.log('Database file was loaded!');

    app.use('/messages', products(fileDb));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
